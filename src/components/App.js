import React from "react";
import SearchBar from "./SearchBar";
import unsplash from "../unsplash";
import ImageList from "./ImageList";

class App extends React.Component {
  // constructor(props){
  //   super(props)
  //   this.state  = {images : []}
  //   this.onSearchSubmit = this.onSearchSubmit.bind(this)
  // }

  state = { images: [] };
  // async  onSearchSubmit   (term) {
  onSearchSubmit = async term => {
    const response = await unsplash.get("/search/photos", {
      params: { query: term }
    });
    console.log(this);
    this.setState({ images: response.data.results });
  };

  render() {
    return (
      <div className="ui container" style={{ marginTop: "10px" }}>
        <SearchBar onSubmit={this.onSearchSubmit} />
        found {this.state.images.length}
        <ImageList images={this.state.images} />
      </div>
    );
  }
}

export default App;
