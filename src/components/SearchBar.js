import React from "react";

class SearchBar extends React.Component {
  state = { term: '' };
  onInputChange = (event)  => {
    this.setState({term: event.target.value})
  }
  onFormSubmit(event){
    
    event.preventDefault();
    console.log(this.props)
    this.props.onSubmit(this.state.term)
    
  }

  render() {
    return (
      <div className="ui segment">
        <form className="ui form" onSubmit={(event) => this.onFormSubmit(event)}>
          <div className="field">
            <label> image search:</label>
            <input type="text" value={this.state.term} onChange={this.onInputChange} />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
